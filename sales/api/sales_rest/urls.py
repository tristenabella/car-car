from django.urls import path

from .views import api_salesperson, api_delete_salesperson, api_customer, api_delete_customer, api_sales, api_delete_sales

urlpatterns = [
    path("salesperson/", api_salesperson, name="api_salesperson"),
    path("salesperson/<int:id>/", api_delete_salesperson, name="api_delete_salesperson"),
    path("customers/", api_customer, name="api_customer"),
    path("customers/<int:id>/", api_delete_customer, name="api_delete_customer"),
    path("sales/", api_sales, name="api_sales"),
    path("sales/<int:id>/", api_delete_sales, name="api_delete_sales")

]
