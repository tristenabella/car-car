import React, {useEffect, useState} from 'react';

function SalesForm() {
    const [automobiles, setAutomobiles] = useState([]);
    const [salespersons, setSalesperson] = useState([]);
    const [customers, setCustomers] = useState([]);

    const [formData, setFormData] = useState ({
        automobile: "",
        salesperson: "",
        customer: "",
        price: "",
    });

    const handleInput = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value
        })
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const response = await fetch ('http://localhost:8090/api/sales/', {
            method: 'POST',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        })

        if (response.ok) {
            const data = await response.json();

            setFormData({
                automobile: "",
                salesperson: "",
                customer: "",
                price: "",
            })
        }
    }

    const fetchAutomobiles = async () => {
        const url = 'http://localhost:8100/api/automobiles/';

        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos)
        }
    }

    const fetchSalesperson = async () => {
        const url = 'http://localhost:8090/api/salesperson/';

        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json();
            setSalesperson(data.salesperson)
        }
    }

    const fetchCustomers = async () => {
        const url = 'http://localhost:8090/api/customers/';
        const response = await fetch(url)

        if(response.ok) {
            const data = await response.json();
            setCustomers(data.customer)
        }
    }


    useEffect(() => {
        fetchAutomobiles();
    },[]);

    useEffect(() => {
        fetchSalesperson();
    },[]);

    useEffect(() => {
        fetchCustomers();
    },[]);


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Record a New Sale</h1>
                    <form onSubmit={handleSubmit} id="create-sales-form">

                        <div className="mb-3">
                            <select onChange={handleInput} value={formData.automobile} required name="automobile" id="automobile" className="form-select">
                                <option value="">Choose an automobile VIN</option>
                                {automobiles?.map(automobile => {
                                    return (
                                        <option key={automobile.id} value={automobile.vin}>
                                            {automobile.vin}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>

                        <div className="mb-3">
                            <select onChange={handleInput} value={formData.salesperson} required name="salesperson" id="salesperson" className="form-select">
                                <option value="">Choose a Salesperson</option>
                                {salespersons?.map(salesperson => {
                                    return (
                                    <option key={salesperson.id} value={salesperson.first_name}>
                                        {salesperson.first_name} {salesperson.last_name}
                                    </option>
                                    )
                                })}
                            </select>
                        </div>

                        <div className="mb-3">
                            <select onChange={handleInput} value={formData.customer} name="customer" id="customer" className="form-selcet">
                                <option value="">Choose a Customer</option>
                                {customers?.map(customer => {
                                    return (
                                        <option key={customer.id} value={customer.first_name}>
                                            {customer.first_name} {customer.last_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>

                        <div className="form-floating mb-3">
                            <input onChange={handleInput} value={formData.price} placeholder="price" required type="number" name="price" id="price" className='form-control'>
                            </input>
                            <label htmlFor="price">Price</label>
                        </div>

                        <button className="btn btn-primary">Create</button>
                   </form>
                </div>
            </div>
        </div>
    )

}

export default SalesForm
