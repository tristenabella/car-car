import React, {useEffect, useState} from 'react';

function SalesPersonForm() {
    const [automobiles, setAutomobiles] = useState([]);
    const [formData, setFormData] = useState ({
        first_name: "",
        last_name: "",
        employee_id:"",
    });

    const handleInput = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value
        })
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const response = await fetch('http://localhost:8090/api/salesperson/', {
            method: 'POST',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        })

        if (response.ok) {
            const data = await response.json();

            setFormData({
                first_name: "",
                last_name:"",
                employee_id: "",
            })
        }

    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/automobiles/';

        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.automobiles)
        }
    }
    useEffect(() => {
        fetchData();
    }, []);



    return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Add a Salesperson</h1>
                <form onSubmit={handleSubmit} id="create-salesperson-form">

                    <div className="form-floating mb-3">
                        <input onChange={handleInput} value={formData.first_name} placeholder='first_name' required type="text" name="first_name" id="first_name" className="form-control"></input>
                        <label htmlFor="first_name">First name</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input onChange={handleInput} value={formData.last_name} placeholder='last_name' required type="text" name="last_name" id="last_name" className="form-control"></input>
                        <label htmlFor="last_name">Last name</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input onChange={handleInput} value={formData.employee_id} placeholder='employee_id' required type="text" name="employee_id" id="employee_id" className="form-control"></input>
                        <label htmlFor="employee_id">Employee ID</label>
                    </div>

                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
    )
}


export default SalesPersonForm
