import React, { useEffect, useState } from 'react';

function ServiceAppointmentsForm() {

  const [formData, setFormData] = useState({
    vin: "",
    customer: "",
    date_time: "",
    reason: "",
    technician: "",
    status: ""
  });

  const [technicians, setTechnicians] = useState([]);

  const fetchData = async() => {
    const response = await fetch('http://localhost:8080/api/technicians/');

    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    } else {
      console.error(response);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleInputChange = (event) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value
    })
  }

  const handleSubmit = async(event) => {
    event.preventDefault();

    const response = await fetch('http://localhost:8080/api/appointments/', {
      method: 'POST',
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json'
      }
    });

    if (response.ok) {
      setFormData({
        vin: "",
        customer: "",
        date_time: "",
        technician: "",
        reason: ""
      })
    } else {
      console.error(response);
    }
  }

  return(
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a service appointment</h1>
          <form onSubmit={handleSubmit} id="create-service-appointment-form">
            <div className="form-floating mb-3">
              <input value={formData.vin} placeholder="Automobile VIN" onChange={handleInputChange} required type="text" name="vin" id="vin" className="form-control" />
              <label htmlFor="vin">Automobile VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input value={formData.customer} placeholder="Customer" onChange={handleInputChange} required type="text" name="customer" id="customer" className="form-control" />
              <label htmlFor="customer">Customer</label>
            </div>
            <div className="form-floating mb-3">
              <input value={formData.date_time} placeholder="Date & Time" onChange={handleInputChange} required type="datetime-local" name="date_time" id="date_time" className="form-control" />
              <label htmlFor="date_time">Date & Time</label>
            </div>
            <div className="mb-3">
              <select value={formData.technician} onChange={handleInputChange} required id="technician" name="technician" className="form-select">
                <option value="">Choose a technician</option>
                  {technicians.map(technician => {
                    return (
                      <option key={technician.employee_id} value={technician.employee_id}>
                        {technician.first_name} {technician.last_name}
                      </option>
                    );
                  })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input value={formData.reason} placeholder="Reason" onChange={handleInputChange} required type="text" name="reason" id="reason" className="form-control" />
              <label htmlFor="reason">Reason</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ServiceAppointmentsForm;
