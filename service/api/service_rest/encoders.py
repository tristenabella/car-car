from common.json import ModelEncoder
from .models import Technician, Appointment, AutomobileVO


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
    ]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
        "import_href",
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "customer",
        "date_time",
        "reason",
        "vin",
        "technician",
        "id",
        "status",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
        "vin": AutomobileVOEncoder()
    }
