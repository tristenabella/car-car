import React, { useEffect, useState } from 'react';

function ModelsForm() {

  const [manufacturers, setManufacturers] = useState([]);
  const [formData, setFormData] = useState({
    name: "",
    picture_url: "",
    manufacturer: "",
  });

  const handleInput = (event) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value
    })
  }

  const handleSubmit = async(event) => {
    event.preventDefault();

    const response = await fetch('http://localhost:8100/api/models/', {
      method: 'POST',
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json'
      }
    });

    if (response.ok) {
      setFormData({
        name: "",
        picture_url: "",
        manufacturer: "",
      })
    } else {
      console.error(response);
    }
  }

  const fetchData = async() => {
    const response = await fetch('http://localhost:8100/api/manufacturers/');

    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    } else {
      console.error(response);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return(
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a vehicle model</h1>
          <form onSubmit={handleSubmit} id="create-model-form">
            <div className="form-floating mb-3">
              <input onChange={handleInput} value={formData.name} placeholder="name" required type="text" name="name" id="name" className="form-control"></input>
              <label htmlFor="name">Model Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleInput} value={formData.picture_url} placeholder="picture_url" required type="text" name="picture_url" id="picture_url" className="form-control"></input>
              <label htmlFor="picture_url">Picture URL</label>
            </div>
            <div className="mb-3">
              <select onChange={handleInput} value={formData.manufacturer} required name="manufacturer" id="manufacturer" className="form-select">
                <option value="">Select a manufacturer</option>
                {manufacturers.map(manufacturer => {
                  return (
                    <option key={manufacturer.id} value={manufacturer.id}>
                      {manufacturer.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ModelsForm;
