import React, { useEffect, useState } from 'react';

function InventoryForm() {

  const [models, setModelsList] = useState([]);
  const [formData, setFormData] = useState({
    color: "",
    year: "",
    vin: "",
    model: "",
  });

  const handleInput = (event) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value
    })
  }

  const handleSubmit = async(event) => {
    event.preventDefault();

    const response = await fetch('http://localhost:8100/api/automobiles/', {
      method: 'POST',
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json'
      }
    });

    if (response.ok) {
      setFormData({
        color:"",
        year: "",
        vin: "",
        model: "",
      })
    } else {
      console.error(response);
    }
  }

  const fetchData = async() => {
    const response = await fetch('http://localhost:8100/api/models/')

    if (response.ok) {
      const data = await response.json();
      setModelsList(data.models);
    } else {
      console.error(response);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add an automobile to inventory</h1>
          <form onSubmit={handleSubmit} id="create-inventory-form">
            <div className="form-floating mb-3">
              <input onChange={handleInput} value={formData.color} placeholder='color' required type='text' name='color' id='name' className="form-control"></input>
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleInput} value={formData.year} placeholder='year' required type='text' name='year' id='year' className="form-control"></input>
              <label htmlFor="year">Year</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleInput} value={formData.vin} placeholder="vin" required type='text' name='vin' id='vin' className="form-control"></input>
              <label htmlFor="vin">Vin</label>
            </div>
            <div className="mb-3">
              <select onChange={handleInput} value={formData.model} required name="model" id="model" className="form-select">
                <option value="">Select a Model</option>
                {models.map(model => {
                  return (
                    <option key={model.id} value={model.id}>
                      {model.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default InventoryForm;
