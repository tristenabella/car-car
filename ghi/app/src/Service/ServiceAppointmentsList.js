import React, { useEffect, useState } from 'react';

function ServiceAppointmentsList() {

  const [appointments, setAppointments] = useState([]);
  const [formData] = useState({
    status: ""
  });

  const handleDate = (date_time) => {
    const time = new Date(date_time).toLocaleDateString();
    return (time);
  }

  const handleTime = (date_time) => {
    const date = new Date(date_time).toLocaleTimeString('en-US', { timeStyle: 'short', hour12: true, timeZone: 'UTC' });
    return (date);
  }

  const handleVIP = (vip) => {
    if (vip) {
      return (<p>Yes</p>);
    } else {
      return (<p>No</p>);
    }
  }

  const fetchData = async() => {
    const response = await fetch('http://localhost:8080/api/appointments/');

    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments.filter(appointment => appointment.status === "created"));
    } else {
      console.error(response);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleCancel = async(e) => {
    const { id } = e.target;
    const response = await fetch(`http://localhost:8080/api/appointments/${id}/cancel/`,{
      method: 'PUT',
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json'
      }
    });
    if (response.ok) {
      fetchData();
    } else {
      console.error(response);
    }
  }

  const handleFinish = async(e) => {
    const { id } = e.target;
    const response = await fetch(`http://localhost:8080/api/appointments/${id}/finish/`,{
      method: 'PUT',
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json'
      }
    });
    if (response.ok) {
      fetchData();
    } else {
      console.error(response);
    }
  }

  return(
    <>
      <h2 className="mt-4">Service Appointments</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {appointments.map(appointment => {
            return (
              <tr key={ appointment.id }>
                <td>{ appointment.vin.vin }</td>
                <td>{ handleVIP(appointment.vin.sold) }</td>
                <td>{ appointment.customer }</td>
                <td>{ handleDate(appointment.date_time) }</td>
                <td>{ handleTime(appointment.date_time) }</td>
                <td>{ appointment.technician.first_name } { appointment.technician.last_name }</td>
                <td>{ appointment.reason }</td>
                <td>
                  <button className="btn btn-danger" id={ appointment.id } onClick={handleCancel}>Cancel</button>
                  <button className="btn btn-success" id={ appointment.id } onClick={handleFinish}>Finish</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default ServiceAppointmentsList;
