import React, { useState } from 'react';

function ManufacturersForm() {

  const [formData, setFormData] = useState({
    name: ""
  });

  const handleInputChange = (event) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value
    })
  }

  const handleSubmit = async(event) => {
    event.preventDefault();

    const response = await fetch('http://localhost:8100/api/manufacturers/', {
      method: 'POST',
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json'
      }
    });

    if (response.ok) {
      setFormData({
        name: ""
      })
    } else {
      console.error(response);
    }
  }

  return(
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a manufacturer</h1>
          <form onSubmit={handleSubmit} id="create-manufacturer-form">
            <div className="form-floating mb-3">
              <input value={formData.name} placeholder="Manufacturer name..." onChange={handleInputChange} required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Manufacturer name...</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ManufacturersForm;
