from django.contrib import admin
from .models import Technician, Appointment, AutomobileVO


@admin.register(Technician)
class TechnicianAdmin(admin.ModelAdmin):
    list_display = (
        "first_name",
        "last_name",
        "employee_id",
    )


@admin.register(AutomobileVO)
class AutomobileVOAdmin(admin.ModelAdmin):
    list_display = (
        "import_href",
        "vin",
        "sold",
    )


@admin.register(Appointment)
class AppointmentAdmin(admin.ModelAdmin):
    list_display = (
        "customer",
        "date_time",
        "reason",
        "status",
        "vin",
        "technician",
    )
