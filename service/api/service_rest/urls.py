from django.urls import path

from .views import (
    api_list_technicians,
    api_show_technician,
    api_list_appointments,
    api_show_appointment,
    api_cancel_appointment,
    api_finish_appointment,
    api_service_history,
)


urlpatterns = [
    path("technicians/", api_list_technicians, name="api_list_technicians"),
    path("technicians/<int:id>/", api_show_technician, name="api_show_technician"),
    path("appointments/", api_list_appointments, name="api_list_locations"),
    path("appointments/<int:id>/", api_show_appointment, name="api_show_location"),
    path("appointments/<int:id>/cancel/", api_cancel_appointment, name="api_cancel_appointment"),
    path("appointments/<int:id>/finish/", api_finish_appointment, name="api_finish_appointment"),
    path("service-history/", api_service_history, name="api_service_history"),
]
