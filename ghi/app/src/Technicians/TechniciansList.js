import React, { useEffect, useState } from 'react';

function TechniciansList() {

  const [technicians, setTechnicians] = useState([]);

  const fetchData = async() => {
    const response = await fetch('http://localhost:8080/api/technicians/');
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    } else {
      console.error(response);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return(
    <>
      <h2 className="mt-4">Technicians</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
          </tr>
        </thead>
        <tbody>
          {technicians.map(technician => {
            return (
              <tr key={ technician.employee_id }>
                <td>{ technician.employee_id }</td>
                <td>{ technician.first_name }</td>
                <td>{ technician.last_name }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default TechniciansList;
