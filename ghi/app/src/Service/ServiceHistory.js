import React, { useEffect, useState } from 'react';

function ServiceHistory() {

  const [appointments, setAppointments] = useState([]);
  const [filterValue, setFilterValue] = useState("");

  const handleDate = (date_time) => {
    const time = new Date(date_time).toLocaleDateString();
    return (time);
  }

  const handleTime = (date_time) => {
    const date = new Date(date_time).toLocaleTimeString('en-US', { timeStyle: 'short', hour12: true, timeZone: 'UTC' });
    return (date);
  }

  const handleVIP = (vip) => {
    if (vip) {
      return (<p>Yes</p>);
    } else {
      return (<p>No</p>);
    }
  }

  const fetchData = async() => {
    const response = await fetch('http://localhost:8080/api/appointments/');

    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
    } else {
      console.error(response);
    }
  }

  const filteredVIN = () => {
    return appointments.filter((appointment) => {
      return (
        appointment.vin.vin.includes(filterValue)
      );
    });
  };

  const handleFilterValueChange = (e) => {
    const { value } = e.target;
    setFilterValue(value);
  };

  useEffect(() => {
    fetchData();
  }, []);

  return(
    <>
      <h2 className="mt-4">Service History</h2>
      <div className="input-group">
        <input className="form-control" onChange={handleFilterValueChange} value={filterValue} placeholder="Search by VIN..." />
        <button className="btn btn-outline-dark" onClick={filteredVIN}>Search</button>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {filteredVIN().map(appointment => {
            return (
              <tr key={ appointment.id }>
                <td>{ appointment.vin.vin }</td>
                <td>{ handleVIP(appointment.vin.sold) }</td>
                <td>{ appointment.customer }</td>
                <td>{ handleDate(appointment.date_time) }</td>
                <td>{ handleTime(appointment.date_time) }</td>
                <td>{ appointment.technician.first_name } { appointment.technician.last_name }</td>
                <td>{ appointment.reason }</td>
                <td>{ appointment.status }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default ServiceHistory;
