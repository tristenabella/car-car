# Car-Car

Car-Car is a car dealership management application that allows service technicians and sales representatives to manage appointments and sales!

## Team

* Tristen Abella: Service microservice
* Drake Phamanivong: Sales microservice

## Service microservice

Tristen Abella

Created a Technician model containing first name, last name, and employee id and created a AutomobileVO model containing vin and if automobiles are sold. Then, created an appointment model containing customer name, date and time, status, and reason. Added to it foreign keys for both Technician and AutomobileVO, which have a one-to-many relationship to the appointment model. The AutomobileVO model polls its data from the inventory microservice. The Technician and Appointment models can be accessed via API to create, display, update, and delete technicians and appointments.

## Sales microservice

Drake Phamanivong

Made a Sale, Customer, and Salesperson model as well as integreting the Automobile and getting the vin with the AutomobileVO.

Then implemented those into API's so we could then delete and Make new Sales, Customers and Salespersons then as we create those we integrate those with the inventory microservices combining them together almost.
Moving onto the frontend we integrated most of the inventory microservices onto their own pages making it visible as well as doing it for the Sales microservices.

## Project Initialization

To fully enjoy this application on your local machine, please make sure to follow these steps:

1. Clone the repository down to your local machine
2. CD into the new project directory
3. Run docker volume create beta-data
4. Run docker compose build
5. Run docker compose up
6. Explore CarCar!
