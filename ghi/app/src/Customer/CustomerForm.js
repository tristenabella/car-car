import React, {useEffect, useState} from 'react';

function CustomerForm() {
    const [automobiles, setAutomobiles] = useState([]);
    const [formData, setFormData] = useState({
        first_name: "",
        last_name: "",
        address: "",
        phone_number: "",
    });


    const handleInput = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value
        })
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const response = await fetch('http://localhost:8090/api/customers/', {
            method: 'POST',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        })

        if (response.ok) {
            const data = await response.json();

            setFormData({
                first_name: "",
                last_name: "",
                address: "",
                phone_number: "",
            })
        }

    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/automobiles/';

        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setAutomobiles(data.automobiles)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Customer</h1>
                    <form onSubmit={handleSubmit} id="create-customer-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleInput} value={formData.first_name} placeholder="first_name" required type="text" name="first_name" id="first_name" className="form-control"></input>
                            <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleInput} value={formData.last_name} placeholder="last_name" required type="text" name="last_name" id="last_name" className="form-control"></input>
                            <label htmlFor="last_name">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleInput} value={formData.address} placeholder="address" required type="address" name="address" id="address" className="form-control"></input>
                            <label htmlFor="address">Address</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleInput} value={formData.phone_number} placeholder="phone_number" required type="phone_number" name="phone_number" id="phone_number" className="form-control"></input>
                            <label htmlFor="phone_number">Phone Number</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )


}
export default CustomerForm
