from django.db import models
from django.urls import reverse


class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.first_name


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin


class Appointment(models.Model):
    customer = models.CharField(max_length=200)
    date_time = models.DateTimeField()
    status = models.CharField(max_length=200)
    reason = models.CharField(max_length=200)

    vin = models.ForeignKey(
        AutomobileVO,
        related_name="appointments",
        on_delete=models.CASCADE,
    )

    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.customer

    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={"pk": self.id})
