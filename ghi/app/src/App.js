import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalesPersonForm from './Salesperson/SalesPersonForm';
import SalesPersonList from './Salesperson/SalesPersonList';
import CustomerForm from './Customer/CustomerForm';
import CustomerList from './Customer/CustomerList';
import SalesForm from './Sales/SalesForm';
import SalesList from './Sales/SalesList';
import SalesHistory from './Sales/SalesHistory';
import TechniciansList from './Technicians/TechniciansList';
import TechniciansForm from './Technicians/TechniciansForm';
import ServiceAppointmentsList from './Service/ServiceAppointmentsList';
import ServiceAppointmentsForm from './Service/ServiceAppointmentsForm';
import ServiceHistory from './Service/ServiceHistory';
import ManufacturersList from './Manufacturers/ManufacturersList';
import ManufacturersForm from './Manufacturers/ManufacturersForm';
import ModelForm from './Models/ModelsForm';
import ModelList from './Models/ModelsList';
import InventoryList from './Inventory/InventoryList';
import InventoryForm from './Inventory/InventoryForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/salesperson">
            <Route index element={<SalesPersonList />} />
            <Route path="create" element={<SalesPersonForm />} />
          </Route>
          <Route path="/customers">
            <Route index element={<CustomerList />} />
            <Route path="create" element={<CustomerForm />} />
          </Route>
          <Route path="/sales">
            <Route index element={<SalesList />} />
            <Route path="create" element={<SalesForm />} />
            <Route path="history" element={<SalesHistory />} />
          </Route>
          <Route path="/technicians">
            <Route index element={<TechniciansList />} />
            <Route path="/technicians/create" element={<TechniciansForm />} />
          </Route>
          <Route path="/appointments">
            <Route index element={<ServiceAppointmentsList />} />
            <Route path="/appointments/create" element={<ServiceAppointmentsForm />} />
            <Route path="/appointments/servicehistory" element={<ServiceHistory />} />
          </Route>
          <Route path="/manufacturers">
            <Route index element={<ManufacturersList />} />
            <Route path="/manufacturers/create" element={<ManufacturersForm />} />
          </Route>
          <Route path="/models">
            <Route index element={<ModelList />} />
            <Route path="/models/create" element={<ModelForm />} />
          </Route>
          <Route path="/automobiles">
            <Route index element={<InventoryList />} />
            <Route path="/automobiles/create" element={<InventoryForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
