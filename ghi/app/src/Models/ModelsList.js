import React, { useEffect, useState } from 'react';

function ModelsList() {

  const [models, setModelsList] = useState([]);

  const fetchData = async() => {
    const response = await fetch('http://localhost:8100/api/models/');

    if (response.ok) {
      const data = await response.json();
      setModelsList(data.models);
    } else {
      console.error(response);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return(
    <>
      <h2 className="mt-4">Models</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {models.map(model => {
            return (
              <tr key={ model.id }>
                <td>{ model.name }</td>
                <td>{ model.manufacturer.name }</td>
                <td><img src={ model.picture_url } alt="Models"></img></td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default ModelsList;
